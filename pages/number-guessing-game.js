document.title = 'number-guessing-game';

let html = `<h1>Игра в угадывание чисел</h1>
<p>Мы выбрали случайное число от 1 до 100. Проверьте, сможете ли вы угадать его за 10 ходов или меньше. <br>
Мы сообщим вам, была ли ваша догадка слишком высокой или слишком низкой.</p>
<div class="form"> <label for="guessField">Введите предположение: </label><input type="text" id="guessField" class="guessField"> <input type="submit" value="Отправить предположение" class="guessSubmit"> </div>
<div class="resultParas">
  <p class="guesses"></p>
  <p class="lastResult"></p>
  <p class="lowOrHi"></p>
  </div>`
newDiv = document.createElement("div");
newDiv.innerHTML = html;

my_div = document.getElementById("org_div1");
document.body.insertBefore(newDiv, my_div);

function checkGuess() {
 var userGuess = Number(guessField.value);
 if (guessCount === 1) {
  guesses.textContent = 'Previous guesses: ';
 }

 guesses.textContent += userGuess + ' ';

 if (userGuess === randomNumber) {
  lastResult.textContent = 'Congratulations! You got it right!';
  lastResult.style.backgroundColor = 'green';
  lowOrHi.textContent = '';
  setGameOver();
 } else if (guessCount === 10) {
  lastResult.textContent = '!!!GAME OVER!!!';
  lowOrHi.textContent = '';
  setGameOver();
 } else {
  lastResult.textContent = 'Wrong!';
  lastResult.style.backgroundColor = 'red';
  if(userGuess < randomNumber) {
   lowOrHi.textContent='Last guess was too low!' ;
  } else if(userGuess > randomNumber) {
   lowOrHi.textContent = 'Last guess was too high!';
  }
 }

 guessCount++;
 guessField.value = '';
}

document.querySelector('.guessSubmit').addEventListener('click', checkGuess);

function setGameOver() {
 guessField.disabled = true;
 guessSubmit.disabled = true;
 resetButton = document.createElement('button');
 resetButton.textContent = 'Start new game';
 document.body.appendChild(resetButton);
 resetButton.addEventListener('click', resetGame);
}

function resetGame() {
 guessCount = 1;
 var resetParas = document.querySelectorAll('.resultParas p');
 for(var i = 0 ; i < resetParas.length ; i++) {
  resetParas[i].textContent='';
 }

 resetButton.parentNode.removeChild(resetButton);
 guessField.disabled = false;
 guessSubmit.disabled = false;
 guessField.value='';
 guessField.focus();
 lastResult.style.backgroundColor='white';
 randomNumber=Math.floor(Math.random() * 100) + 1;
}
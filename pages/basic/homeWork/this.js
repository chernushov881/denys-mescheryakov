document.title = 'this'

/*
Создать объект, который описывает ширину и высоту
прямоугольника, а также может посчитать площадь фигуры:
const rectangle = {width:..., height:..., getSquare:...};
*/
const rectangle = {
    width: 100,
    height: 200,
    getSquare(w, h) {
        w = w || this.width
        h = h || this.height
        return w * h
    }
}
console.log(rectangle.getSquare(15, 3))
console.log(rectangle.getSquare())

/*
Создать объект, у которого будет цена товара и его скидка, а также
два метода: для получения цены и для расчета цены с учетом скидки:
const price = {
    price: 10,
    discount: '15%',
... };
price.getPrice(); // 10
price.getPriceWithDiscount(); // 8.5
*/
const price = {
    price: 10,
    discount: '15%',
    getPrice() {
        return this.price
    },
    getPriceWithDiscount() {
        //return this.price - (this.price * (parseInt(this.discount)/100))
        return this.price - this.price / 100 * parseInt(this.discount)
    }
};
price.getPrice() // 10
price.getPriceWithDiscount() // 8.5

/*
Создать объект, у которого будет поле высота и метод “увеличить
высоту на один”. Метод должен возвращать новую высоту:
object.height = 10;
object.inc(); // придумать свое название для метода
object.height; // 11;
*/
const object = {
    height: 10,
    increaseHeight() {
        this.height += 1
        return this.height;
    }
}
object.height
object.increaseHeight()
object.height

/**
 *
 Создать объект “вычислитель”, у которого есть числовое свойство
 “значение” и методы “удвоить”, “прибавить один”, “отнять один”.
 Методы можно вызывать через точку, образуя цепочку методов:
 */

const numerator = {
    value: 1,
    double() {
        this.value *= 2
        return this
    },
    plusOne() {
        this.value += 1
        return this
    },
    minusOne() {
        this.value -= 1
        return this
    }
}
numerator.double().plusOne().plusOne().minusOne()
numerator.value

/*
Создать объект с розничной ценой и количеством продуктов.
Этот объект должен содержать метод для получения общей стоимости всех товаров (цена * количество продуктов)
*/
const product = {
    price: 100,
    count: 5,
    allPriceProduct(p, c) {
        p = p || this.price
        c = c || this.count
        return p * c
    }
}
product.allPriceProduct(15, 5)
/*
Создать объект из предыдущей задачи.
Создать второй объект, который описывает количество деталей и цену за одну деталь.
Для второго объекта нужно узнать общую стоимость всех деталей, но нельзя создавать новые функции и методы.
Для этого “позаимствуйте” метод из предыдущего объекта.
 */

const product2 = {
    price: 100,
    count: 5
}
product2.allPriceProduct = product.allPriceProduct
product2.allPriceProduct(50, 20)

/*
Даны объект и функция:
let sizes = {width: 5, height: 10},
getSquare = function () {return this.width * this.height};
Не изменяя функцию или объект, получить результат функции getSquare для объекта sizes
 */
let sizes = {
        width: 5,
        height: 10
    },
    getSquare = function () {
        return this.width * this.height
    }

getSquare.call(sizes)
getSquare.apply(sizes)
getSquare.bind(sizes)()
/*
Измените функцию getElementHeight таким образом, чтобы можно было вызвать getElementHeight() и получить 25.
 */

let element = {
    height: 25,
    getHeight: function () {
        return this.height;
    }
};

let getElementHeight = element.getHeight;
getElementHeight(); // undefined
getElementHeight.call(element)
getElementHeight.apply(element)
getElementHeight.bind(element)()
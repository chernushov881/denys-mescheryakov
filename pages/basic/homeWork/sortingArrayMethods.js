document.title = 'sortingArrayMethods.js'

/*
1 На основе массива [1,2,3,5,8,9,10] сформировать новый массив,
каждый элемент которого будет хранить информацию о числе и его четности:
[{digit: 1, odd: true}, {digit: 2, odd: false}, {digit: 3, odd: true}...]
 */
const newArray = [1, 2, 3, 5, 8, 9, 10].map((elem, index, array) => ({
		digit: index + 1,
		odd: index % 2 !== 0
	})
)
//console.log(newArray)
const initialValue0 = [1,2,3,5,8,9,10].map(num => ({
	value: num,
	odd: Boolean(num % 2)
}));
//console.log(initialValue0)
/*
2 Проверить, содержит ли массив [12, 4, 50, 1, 0, 18, 40] элементы, равные нулю. Если да - вернуть true.
 */
const isElemZero = [12, 4, 50, 1, 0, 18, 40].some(el => el === 0)
//console.log(isElemZero)

const initialValue2 = [12, 4, 50, 1, 0, 18, 40];
const zeroExist = initialValue2.some(num => num === 0);
//console.log(zeroExist)
/*
3 Проверить, все элементы массива имеют длину более 3х символов ['yesd', 'hello', 'nodr', 'easycode', 'what']. Если да - вернуть true
 */
const moreElemThree = ['yes', 'hello', 'no', 'easycode', 'what'].every(el => el.length > 3)
//console.log(moreElemThree)
/*
4 Дан массив объектов,
где каждый объект содержит информацию о букве и месте её положения в строке {буква: “a”, позиция_в_предложении: 1}:

Напишите функцию, которая из элементов массива соберет и вернёт
строку, основываясь на index каждой буквы. Например:
[{char:"H",index:0}, {char:"i",index: 1}, {char:"!",index:2}] → “Hi!”
 */
const resStr = [
	{char: "a", index: 12},
	{char: "w", index: 8},
	{char: "Y", index: 10},
	{char: "p", index: 3},
	{char: "p", index: 2},
	{char: "N", index: 6},
	{char: " ", index: 5},
	{char: "y", index: 4},
	{char: "r", index: 13},
	{char: "H", index: 0},
	{char: "e", index: 11},
	{char: "a", index: 1},
	{char: " ", index: 9},
	{char: "!", index: 14},
	{char: "e", index: 7}]
// resStr
// 	.sort((prev, next) => prev.index - next.index)
// 	.map(el => el.char)
// 	.join('')
//console.log(resStr)



function getStr(arr) {
	if (!Array.isArray(arr)) return
	const cloned = arr.slice();
	return cloned
		.sort((prev, next) => prev.index - next.index)
		.reduce((acc, { char }) => acc + char, '');
}
console.log(getStr(resStr))

function getStr2(){
	const clone = arr.slice()
	return {
		clone
	}
}
/*let resStr2 = resStr.reduce((acc, el) => {
  acc[el.index] = el.char
  return acc.join('')
})*/
// console.log(resStr2)
/*
Отсортируйте массив массивов так, чтобы вначале располагались наименьшие массивы (размер массива определяется его длиной):
[  [14, 45],  [1],  ['a', 'c', 'd']  ] → [ [1], [14, 45], ['a', 'c', 'd'] ]
 */
let arrArrs = [[14, 45], [1], ['a', 'c', 'd']]
arrArrs.sort((prev, next) => prev.length - next.length)
//console.log(arrArrs)

/*
Есть массив объектов:
Отсортировать их по возрастающему количеству ядер (cores).
*/
let arrObjectInfo = [
	{cpu: 'intel', info: {cores: 2, сache: 3}},
	{cpu: 'intel', info: {cores: 4, сache: 4}},
	{cpu: 'amd', info: {cores: 1, сache: 1}},
	{cpu: 'intel', info: {cores: 3, сache: 2}},
	{cpu: 'amd', info: {cores: 4, сache: 2}}
]
arrObjectInfo.sort((prev, next) => prev.info.cores - next.info.cores)
//console.log(arrObjectInfo)

/*
3. Создать функцию, которая будет принимать массив продуктов и две цены.
Функция должна вернуть все продукты, цена которых находится в указанном диапазоне, и сортировать от дешевых к дорогим:
 */
let products = [
	{title: 'prod1', price: 5.2}, {title: 'prod2', price: 0.18},
	{title: 'prod3', price: 15}, {title: 'prod4', price: 25},
	{title: 'prod5', price: 18.9}, {title: 'prod6', price: 8},
	{title: 'prod7', price: 19}, {title: 'prod8', price: 63}
];
function filterCollection(arr, minPrice, maxPrice){
	return arr
		.slice()
		.sort((prev, next) => prev.price - next.price)
		.filter( elem => elem.price <= maxPrice && elem.price >= minPrice )
}

console.log(filterCollection(products, 15, 30)) //→ [{...price: 15}, {...price: 18.9}, {...price: 19}, {...price: 25}]

document.title = 'hw if-statement';

// >, <, >=, <=, ==, ===, !=, !==

/*
Вопросы к этому заданию

Чему равно а, почему?
*/
/*
let a = 0 || 'string';console.log(a)
a = 1 && 'string'; console.log(a)
a = null || 25; console.log(a)
a = null && 25; console.log(a)
a = null || 0 || 35; console.log(a)
a = null && 0 && 35; console.log(a)
*/
/*
Что отобразится в консоли. Почему?
*/
/*console.log(12 + 14 + '12')//2612
console.log(3 + 2 - '1')//4
console.log('3' + 2 - 1)//31
console.log(true + 2)//3
console.log(+'10' + 1)//11
console.log(undefined + 2) //NaN
console.log(null + 5) //5
console.log(true + undefined) //NaN*/


/*Создать произвольную переменную, присвоеть ей значение и написать условие, если переменная равна “hidden”,
 присвоить ей значение “visible”, иначе - “hidden”.

Создать переменную и присвойте ей число.
Используя if, записать условие:
- если переменная равна нулю, присвоить ей 1;
- если меньше нуля - строку “less then zero”;
- если больше нуля - используя оператор “присвоение”, переменную умножить на 10 (использовать краткую запись).
Дан объект let car = { name: 'Lexus', age: 10, create: 2008, needRepair: false }.
Написать условие если возраст машины больше 5 лет то нужно вывести в консоль сообщение 'Need Repair' и свойство needRepair в объекте car изменить на true; иначе изменить на false.
Дан объект let item = { name: 'Intel core i7', price: '100$', discount: '15%' }.
Написать условие если у item есть поле discount и там есть значение которое не NaN а также есть поле price значение которого также не NaN то в объекте item создать поле priceWithDiscount и записать туда цену с учетом скидки и вывести ее в консоль, обратите внимание что поля discount и price это строки и вам из них нужно получить числа чтобы выполнить расчет. иначе если поля discount нет то вывести просто поле price в консоль.


Дан следующий код:
let product = {
name: “Яблоко”,
price: “10$”
};


let min = 10; // минимальная цена
let max = 20; // максимальная цена


Написать условие если цена товара больше или равна минимальной цене и меньше или равна максимальной цене то вывести в консоль название этого товара, иначе вывести в консоль что товаров не найдено.
 */
//Чему равно а, почему?
/*let a = 0 || 'string'; // 'string'возвращает первое попавшееся true
a = 1 && 'string'; // 'string' , возвращает первый попавшийся false или последнее true
a = null || 25; // 25 , возвращает первое попавшееся true
a = null && 25; //null возвращает первое попавшееся false
a = null || 0 || 35; //35 возвращает первое попавшееся true
a = null && 0 && 35; //null возвращает первый попавшийся false
*///Что отобразится в консоли. Почему?
12 + 14 + '12' // 2612  сначала происходит сложение 12 + 14 после происходит конкатенация со строкой

3 + 2 - '1' //4 При любых математических операциях со строкой кроме сложения строка преобразуется к числу поэтому пры вычитании строки "1" она преобразовалась в число 1
'3' + 2 - 1 //31 сначала произошла конкатенация и получилась строка "32" и при вычитании получили число 31
true + 2 //3 true при числовом преобразовании становится 1
+'10' + 1 // 11 унарный плюс превращает строку в число и происходит сложение чисел
undefined + 2 // NaN при любый математических операциях с undefined кроме сложения со строкой, результат будет NaN
null + 5 // 5 null преобразуется к нулю
true + undefined // NaN при любый математических операциях с undefined кроме сложения со строкой, результат будет NaN
//Создать произвольную переменную, присвоеть ей значение и написать условие, если переменная равна “hidden”, присвоить ей значение “visible”, иначе - “hidden”.
let elem
if (elem === 'hidden'){
  elem = 'visible'
}else{
  elem = 'hidden'
}
elem = (elem === 'hidden') ? 'visible' : 'hidden'; console.log(elem)
elem = (elem === 'hidden') ? 'visible' : 'hidden'; console.log(elem)
// Создать переменную и присвойте ей число.
// Используя if, записать условие:
// - если переменная равна нулю, присвоить ей 1; - если меньше нуля - строку “less then zero”; - если больше нуля - используя оператор “присвоение”, переменную умножить на 10 (использовать краткую запись).
let numb = 5
if (numb === 0){
  numb = 1
}else if(numb < 0){
  numb = 'less then zero'
}else if(numb > 0){
  numb *= 10
}
/*
Дан объект let car = { name: 'Lexus', age: 10, create: 2008, needRepair: false }.
Написать условие если возраст машины больше 5 лет то нужно вывести в консоль сообщение 'Need Repair' и свойство needRepair в объекте car изменить на true; иначе изменить на false.*/
let car = {
  name: 'Lexus',
  age: 10,
  create: 2008,
  needRepair: false
}
if (car.age > 5){
  console.log('Need Repair')
  car.needRepair = true
  console.log(car.needRepair)
}else{
  car.needRepair = false
  console.log(car.needRepair)
}
/*Дан объект let item = { name: 'Intel core i7', price: '100$', discount: '15%' }.
Написать условие если у item есть поле discount и там есть значение которое не NaN
а также есть поле price значение которого также не NaN
то в объекте item создать поле priceWithDiscount и записать туда цену с учетом скидки и вывести ее в консоль,
 обратите внимание что поля discount и price это строки и вам из них нужно получить числа чтобы выполнить расчет. иначе если поля discount нет то вывести просто поле price в консоль.
*/
let item = {
  name: 'Intel core i7',
  price: '100$',
  discount: '15%'
}
console.log(parseFloat(item.price) )
console.log(Number(item.price.slice(0,-1)))
if (!item.discount.isNaN && !item.price.isNaN){
  item.priceWithDiscount = Number(item.price.slice(0,-1)) * Number(item.discount.slice(0,-1))/100
  //parseInt(item.price.slice(0,-1)) * parseInt(item.discount.slice(0,-1))/100
  console.log(item.priceWithDiscount)
}else if (item.hasOwnProperty('discount')){
  console.log(item.price)
}
// мы выносим цену и скидку в отдельные переменные что бы несколько раз не вызывать parseFloat
const price = parseFloat(item.price);
const discount = parseFloat(item.discount);
// проверяем что цена и скинда не являются NaN так как это важно при расчетах
if (!isNaN(price) && !isNaN(discount)) {
  const priceWithDiscount = price - (price * (discount / 100));
  console.log(item.priceWithDiscount)
  console.log(priceWithDiscount)
} else {
  console.log(price);
}
/*
* Дан следующий код:
Написать условие
если цена товара больше или равна минимальной цене и меньше или равна максимальной цене то вывести в консоль название этого товара, иначе вывести в консоль что товаров не найдено.*/
let product = {
  name: 'Яблоко',
  price: '10$'
}
let min = 10 // минимальная цена
let max = 20 // максимальная цена
if(Number(product.price.slice(0,-1)) >= min && Number(product.price.slice(0,-1)) <= max ){
  console.log(product.name)
}else{
  console.log('товаров не найдено.')
}
product.name = 'Яблоко 2'
const productPrice = parseFloat(product.price);
if (productPrice >= min && productPrice <= max) {
  console.log(product.name);
} else {
  console.log('товаров не найдено');
}






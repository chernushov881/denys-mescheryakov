document.title = 'hw array-methods.js';
/*Создать функцию multiply, которая будет принимать любое количество чисел и возвращать их произведение: multiply(1,2,3) = 6 (1*2*3)

Если нет ни одного аргумента, вернуть ноль: multiply() // 0*/
function multiply(...args){
  if (!arguments.length) return 0;
  let res = 1;

  for (let i = 0; i < arguments.length; i++) {
    res *= arguments[i];
  }

  return res;
}
multiply(1,2,3)

/*
Создать функцию, которая принимает строку и возвращает строку-перевертыш: reverseString(‘test’) // “tset”.
*/
function reverseString(str){
  /*let string = String(str),
    res = ''
  for (let i = string.length; i--; ) {
    res += string[i]
  }
  return res*/
  let res = String(str).split('')
  let nev =  res.reverse()
  return nev.join('')
}

console.log(reverseString('test123'))
 /*
 Создать функцию, которая в качестве аргумента принимает строку из букв и возвращает строку, где каждый символ разделен пробелом и заменен на юникод-значение символа:

getCodeStringFromText(‘hello’) // “104 101 108 108 111”

подсказка: для получения кода используйте специальный метод
 */
function getCodeStringFromText(str){
  let string = String(str),
      res = ''
  for (let i = 0; i < string.length; i++) {
    res += string[i].charCodeAt() + ' '
  }
  return res.trim()
}
getCodeStringFromText('hello')

/*
Создать функцию угадай число. Она принимает число от 1-10 (обязательно проверить что число не больше 10 и не меньше 0). Генерирует рандомное число от 1-10 и сравнивает с заданным числом если они совпали то возвращает “Вы выиграли” если нет то “Вы не угадали ваше число 8 а выпало число 5”. Числа в строке указаны как пример вы подставляете реальные числа.
*/
function guessTheNumber(num) {
  const number = Number(num);

  if (typeof number !== "number" || isNaN(number)) return new Error("Please provide a valid number");
  if (number < 0 || number > 10) return new Error("Please provide number in range 0 - 10");

  const random = Math.ceil(Math.random() * 10);

  if (random === number) return "You win!";

  return `You are lose, your number is ${number}, the random number is ${random}`;
}

//console.log(guessTheNumber(21));
/*
5. Создать функцию, которая принимает число n и возвращает массив, заполненный числами от 1 до n: getArray(10); // [1,2,3,4,5,6,7,8,9,10]
Данное задание выполните после того как познакомитесь с методами массивов.
*/
function getArray(num){
  const arg = [].push(num)
}
/*
 6. Создать функцию, которая принимает массив, а возвращает новый массив с дублированными элементами входного массива. Данное задание выполните после того как познакомитесь с методами массивов:
 doubleArray([1,2,3]) // [1,2,3,1,2,3]
 */
function doubleArray(arr) {
  return arr.concat(arr)
}
console.log(doubleArray([1,2,3])) // [1,2,3,1,2,3]

function doubleArray2(arr) {
  return arr.map( item => item)
  return arr.slice(arr)
}

console.log(doubleArray2([1,2,3,4])) // [1,2,3,1,2,3]

/*
 7. Создать функцию, которая принимает произвольное (любое) число массивов и удаляет из каждого массива первый элемент, а возвращает массив из оставшихся значений. Данное задание выполните после того как познакомитесь с методами массивов:
 changeCollection([1,2,3], [‘a’, ’b’, ‘c’]) → [ [2,3], [‘b’, ‘c’] ], changeCollection([1,2,3]) → [ [2,3] ] и т.д.
 */
function changeCollection() {
  const res = []
  console.log(arguments)
  for (let i = 0; i < arguments.length; i++) {
    if (Array.isArray(arguments[i])){
      const el = arguments[i].slice()
      el.shift()
      res.push(el)
    }
  }
  return res
}
console.log(changeCollection([1,2,3]))
console.log(changeCollection([1,2,3,4], ['a','b','c']))
/*
 8. Создать функцию которая принимает массив пользователей, поле на которое хочу проверить и значение на которое хочу проверять. Проверять что все аргументы переданы. Возвращать новый массив с пользователями соответсвующие указанным параметрам.
 funcGetUsers(users, “gender”, “male”); // [ {name: “Denis”, age: “29”, gender: “male”} , {name: “Ivan”, age: “20”, gender: “male”} ]
 */
const users = [
  {
    "_id": "5e36b779dc76fe3db02adc32",
    "balance": "$1,955.65",
    "picture": "http://placehold.it/32x32",
    "age": 33,
    "name": "Berg Zimmerman",
    "gender": "male"
  },
  {
    "_id": "5e36b779d117774176f90e0b",
    "balance": "$3,776.14",
    "picture": "http://placehold.it/32x32",
    "age": 37,
    "name": "Deann Winters",
    "gender": "female"
  },
  {
    "_id": "5e36b779daf6e455ec54cf45",
    "balance": "$3,424.84",
    "picture": "http://placehold.it/32x32",
    "age": 36,
    "name": "Kari Waters",
    "gender": "female"
  }
]


function filterUsers(arr, key, value) {
  if(!Array.isArray(arr)) return new Error('The first argument should be an array')
  if (typeof key !== 'string' || key === '') return new Error('The key should be a valid string')
  if (value === undefined || value === null) return new Error('The value should be a valid value')
  const res = []
  for (let i = 0; i < arr.length; i++) {
    if (arr[i][key] === value) {
      res.push(arr[i])
    }
  }
  return res
}

console.log(filterUsers(users, "gender", "female"))


document.title = 'number';



/*
Получить число pi из Math и округлить его до 2-х знаков после точки


Используя Math, найти максимальное и минимальное числа из представленного ряда 15, 11, 16, 12, 51, 12, 13, 51

Работа с Math.random:

a. Получить случайное число и округлить его до двух цифр после запятой

b. Получить случайное целое число от 0 до X. X - любое произвольное число.


  Проверить результат вычисления 0.6 + 0.7 - как привести к нормальному виду (1.3)?

  Получить число из строки ‘100$’

*/
//Получить число pi из Math и округлить его до 2-х знаков после точки
let numberPi = Number(Math.PI.toFixed(2))
console.log(numberPi)
//Используя Math, найти максимальное и минимальное числа из представленного ряда 15, 11, 16, 12, 51, 12, 13, 51
let arr = [15, 11, 16, 12, 51, 12, 13, 51],
arrMin = Math.min.apply(null, arr),
arrMax = Math.max.apply(null, arr)
console.log(arrMin)
console.log(arrMax)
//a. Получить случайное число и округлить его до двух цифр после запятой
console.log(Number(Math.random().toFixed(2))) // or
console.log(parseFloat(Math.random().toFixed(2)))
//b. Получить случайное целое число от 0 до X. X - любое произвольное число.
console.log(Number(Math.random().toFixed(1) * 10))
console.log(parseFloat(Math.random().toFixed(1) * 10))
const X = 20;
const randomX = Math.round(Math.random() * X);
console.log(randomX)
//Проверить результат вычисления 0.6 + 0.7 - как привести к нормальному виду (1.3)?
Number(0.6*10 +0.7*10)/10
Number(0.6 + 0.7).toFixed(1)
//Получить число из строки ‘100$’
console.log(parseFloat('100$'))
console.log(parseInt('100$'))
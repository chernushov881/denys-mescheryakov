document.title = 'hw String';



/*
Дана строка: let string = "some test string";

ВРУЧНУЮ НИЧЕГО НЕ СЧИТАТЬ


Вопросы к этому заданию

Получить первую и последнюю буквы строки

Сделать первую и последнюю буквы в верхнем регистре

Найти положение слова ‘string’ в строке

Найти положение второго пробела (“вручную” ничего не считать)

Получить строку с 5-го символа длиной 4 буквы

Получить строку с 5-го по 9-й символы

Получить новую строку из исходной путем удаления последних 6-и символов (то есть исходная строка без последних 6и символов)


Из двух переменных a=20 и b=16 получить переменную string, в которой будет содержаться текст “2016”

*/

let string = "some test string";
let value;
// Получить первую и последнюю буквы строки
value = string[0]
value = string[string.length - 1]
//Сделать первую и последнюю буквы в верхнем регистре
value = string[0].toUpperCase()
value = string[string.length - 1].toUpperCase()
const firstLastUpper = `${string[0].toUpperCase()}${string.slice(1,-1)}${string[string.length - 1].toUpperCase()}`;
value = console.log(firstLastUpper)
//Найти положение слова ‘string’ в строке
value = console.log(string.indexOf('string'))
//Найти положение второго пробела (“вручную” ничего не считать)
console.log(string.indexOf(' ', string.indexOf(' ') + 1))
//Получить строку с 5-го символа длиной 4 буквы
console.log(string.slice(5, (5 + 7)))
substr = string.substr(5, 6);
console.log(substr)
// Получить строку с 5-го по 9-й символы
console.log(string.slice(5, 9))
//Получить новую строку из исходной путем удаления последних 6-и символов (то есть исходная строка без последних 6и символов)
let newString = string.slice(0, -6)
//Из двух переменных a=20 и b=16 получить переменную string, в которой будет содержаться текст “2016”
let a = 20,
    b = 16
let string2 = a + '' + b,
  string3 = String(a) + String(b);

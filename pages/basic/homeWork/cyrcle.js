document.title = 'cyrcle HW';
/**
 В данных задачах старайтесь не использовать методы массивов. Используйте циклы и условия.
 Вопросы к этому заданию*/

/*
 На основе строки “i am in the easycode” сделать новую строку где первые буквы каждого слова будут в верхнем регистре. Использовать for или while.
 
 let str0 = 'i am in the easycode'
 let word = []
 let arr0 = str0.split(' ')
 for (let i = 0; i < arr0.length; i++) {
 let result = arr0[i][0].toUpperCase() + arr0[i].slice(1, arr0[i].length) + ' '
 word += result
 }
 word = word.slice(0, -1)
 console.log(word)
 */
/*
 
 const courseStr = 'i am in the easycode'
 let newCourseStr = ''
 for (let i = 0; i < courseStr.length; i++) {
 if (courseStr[i - 1] === '' || i === 0) {
 newCourseStr += courseStr[i].toUpperCase()
 } else {
 newCourseStr += courseStr[i]
 }
 }
 console.log(newCourseStr)
 */
/*-------------------------
 let res = '';
 let str = 'i am in the easycode'
 for (let i = 0; i < str.length; i++) {
 if (i === 0 || str[i - 1] === ' ') {
 res += str[i].toUpperCase();
 } else {
 res += str[i];
 }
 }
 console.log(res);
 */

/*
 Дана строка “tseb eht ma i”. Используя циклы, сделать строку-перевертыш (то есть последняя буква становится первой, предпоследняя - второй итд).
 
 let str2 = 'tseb eht ma i'
 let newStr = ''
 for ( i = str2.length - 1; --i;) {
 newStr += str2[i]
 }
 console.log(newStr) */
/*
 Факториал числа - произведение всех натуральных чисел от 1 до n
 включительно: 3! = 3*2*1, 5! = 5*4*3*2*1. С помощью циклов вычислить факториал числа 10. Использовать for.
 
 let tenFactorial = 1
 for (let i = 1; i <= 4; i++) {
 tenFactorial *= i
 }
 console.log(tenFactorial) */
/*
 На основе строки “JavaScript is a pretty good language” сделать новую строку, где каждое слово начинается с большой буквы, а пробелы удалены. Использовать for.
 */
/*let strJs = 'JavaScript is a pretty good language',
 wordJs = [],
 arrJs = strJs.split(' ')
 console.log(arrJs)
 for (let i = 0; i < arrJs.length; i++) {
 let res = arrJs[i][0].toUpperCase() + arrJs[i].slice(1, arrJs[i].length)
 wordJs += res
 }
 console.log(wordJs)
 let str = 'JavaScript is a pretty good language'
 let res = '';
 for (let i = 0; i < str.length; i++) {
 if (i === 0 || str[i - 1] === ' ') {
 res += str[i].toUpperCase();
 } else if (str[i] !== ' '){
 res += str[i];
 }
 }
 console.log(res);*/
/*
 Найти все нечетные числа в массиве от 1 до 15 включительно и вывести их в консоль. Массив [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15] Использовать for of.
 
 let arrNum = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
 newArrNum = [],
 tet = ''
 for (let i = 0; i < arrNum.length; i++) {
 if (i % 2 !== 0) {
 newArrNum.push(i)
 }
 }
 console.log(newArrNum) */
/*let arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
 newArrNum = []
 for (let num of arr) {
 if (num % 2) {
 newArrNum.push(num)
 }
 }
 console.log(newArrNum)*/
/*
 Перебрать объект и если значение в свойстве это строка то переписать ее всю в верхнем регистре. Использовать for in.
 */
let list = {
 name: "Sergey",
 work: 'easycode',
 age: 33
}
for (let key in list) {
 if (typeof list[key] != 'number') {
	// if (key typeof == 'Number'){
	// console.log('test')
	// }
	list[key] = list[key].toUpperCase()
 } else if (typeof list[key] == 'number') {
	list[key] = list[key] * list[key]
 }
}
//console.log(list)

var x = 1;
if (x) {
 var x = 2;
 //console.log(x);
}
//console.log(x);

/*
 var href      = document.location.href,
 new_url   = href.split('?')[1],
 url       = href.split('?')[0],
 arr2       = new_url.split('&'),
 newObject = {}
 console.log(href)
 
 for (key of arr2) {
 if (key.split('=')[0] === 'utm_source'){
 let utm_source = key.split('=')[1]
 } else if (key.split('=')[0] === 'utm_medium'){
 let utm_medium = key.split('=')[1]
 } else if (key.split('=')[0] === 'utm_campaign'){
 let utm_campaign = key.split('=')[1]
 } else if (key.split('=')[0] === 'utm_content'){
 let utm_content = key.split('=')[1]
 } else if (key.split('=')[0] === 'utm_term'){
 let utm_term = key.split('=')[1]
 }
 newObject[key.split('=')[0]] = key.split('=')[1]
 }
*/
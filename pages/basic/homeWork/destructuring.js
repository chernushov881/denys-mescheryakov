document.title = 'destructuring'

/*
 1 Вопросы к этому заданию
 Используя rest оператор и деструктуризацию, создать функцию, которая принимает любое количество аргументов и возвращает объект,
 содержащий первый аргумент и массив из остатка:
 */

function func(first, ...other) {
	return {
		first, other
	}
}

func('a', 'b', 'c', 'd') //{ first: 'a', other: ['b','c','d'] }


/*
 2 Организовать функцию getInfo, которая принимает объект вида
 { name: ..., info: { employees: [...], partners: [ … ] } }
 и выводит в консоль имя (если имени нет, показывать ‘Unknown’) и первые две компании из массива partners:
 */
const organisation = {
	name: 'Google',
	info: {
		employees: ['Vlad', 'Olga'],
		partners: ['Microsoft', 'Facebook', 'Xing']
	}
}
function getInfo(obj){
  const name = obj.name
 	const {
	 info:{
		partners:[a,b]
	 }
	} = obj
	return `Name: ${name} Partners: ${a} ${b}`
}

getInfo(organisation)
 
// Name: Google
// Partners: Microsoft Facebook


/*
 3 Дан объект:
 Используя деструктуризацию получить значения из следующих полей
 
 1. name,  balance, email
 
 2. из массива tags получить первый и последний элемент
 
 3. из массива friends получить значение поле name из первого элемента массива
 
 Если какое то из полей не имеет значения то подставить значение по умолчанию
 */
let user = {
	"guid": "dd969d30-841d-436e-9550-3b0c649e4d34",
	"isActive": false,
	"balance": "$2,474.46",
	"age": 30,
	"eyeColor": "blue",
	"name": "Tameka Maxwell",
	"gender": "female",
	"company": "ENOMEN",
	"email": "tamekamaxwell@enomen.com",
	"phone": "+1 (902) 557-3898",
	"tags": [
		"aliquip",
		"anim",
		"exercitation",
		"non",
	],
	"friends": [
		{
			"id": 0,
			"name": "Barber Hicks"
		},
		{
			"id": 1,
			"name": "Santana Cruz"
		},
		{
			"id": 2,
			"name": "Leola Cabrera"
		}
	],
	
};

const{
	name = '',
	email = '',
	balance = '',
	tags = [firstTag = '', , , lastTag = ''] ,
	friends = [{ name: friendName = '' }]
} = user

//console.log(name, balance, email, tags[0], tags[tags.length - 1], friends[0].name)

 
 /*4 .
 
 С помощью оператора rest, из объекта user (из предыдущей задачи) скопировать в новый массив значение следующих полей tags и friends.
 */

const newArr =  [...user.tags, ...user.friends]

console.log(newArr)
document.title = 'hw Objects';



/*
Все поля добавлять по очереди, не создавать сразу готовый объект со всеми полями.
Вопросы к этому заданию

Создать объект с полем product, равным ‘iphone’

Добавить в объект поле price, равное 1000 и поле currency, равное ‘dollar’

Добавить поле details, которое будет содержать объект с полями model и color
*/

let readyObject = {}
readyObject.product = 'iphone'
readyObject.price = 1000
readyObject.currency = 'dollar'
readyObject.details = {}
readyObject.details.model = '13 Pro Max'
readyObject.details.color = 'black'
console.log(readyObject)
document.title = 'callback'
/*
Создайте функцию которая бы умела делать:
minus(10)(6); // 4
minus(5)(6); // -1
minus(10)(); // 10
minus()(6); // -6
minus()(); // 0
Подсказка, функция minus должна возвращать другую функцию.
 */
function minus(numbFirst = 0) {
  return (numbLast = 0) => numbFirst - numbLast
}
const minus2 = (numbFirst = 0) => (numbLast = 0) => numbFirst - numbLast
minus(10)(6); // 4
minus(5)(6); // -1
minus(10)(); // 10
minus()(6); // -6
minus()(); // 0
/*
Реализовать функцию, которая умножает и умеет запоминать возвращаемый результат между вызовами:
function multiplyMaker ...
const multiply = multiplyMaker(2);
multiply(2); // 4 (2 * 2)
multiply(1); // 4 (4 * 1)
multiply(3); // 12 (4 * 3)
multiply(10); // 120 (12 * 10)
 */
function multiplyMaker(firstNumb = 0){
  let num = firstNumb
  return (lastNimb = 0) => num *= lastNimb
}
const multiply = multiplyMaker(2);
multiply(2); // 4 (2 * 2)
multiply(1); // 4 (4 * 1)
multiply(3); // 12 (4 * 3)
multiply(10); // 120 (12 * 10)
/*
3. Реализовать модуль, который работает со строкой и имеет методы:
a. установить строку
i. если передано пустое значение, то установить пустую строку
ii. если передано число, число привести к строке
b. получить строку
c. получить длину строки
d. получить строку-перевертыш
Пример:
модуль.установитьСтроку(‘abcde’);
модуль.получитьСтроку(); // ‘abcde’
модуль.получитьДлину(); // 5
 */
function modulStringFunc(){
  let str = ''
  return {
    setString(val = '') {
      str = String(val)
    },
    getString() {
      return str
    },
    getStringLength() {
      return str.length
    },
    getStringRevers() {
      return str.split('').reverse().join('')
    }
  }
}
const modulString = modulStringFunc()
console.log(modulString.setString('qqqqq'))
console.log(modulString.getString())
console.log(modulString.getStringLength())
console.log(modulString.getStringRevers())

/*
4. Создайте модуль “калькулятор”, который умеет складывать, умножать, вычитать, делить и возводить в степень.
Конечное значение округлить до двух знаков после точки (значение должно храниться в обычной переменной, не в this).

модуль.установитьЗначение(10); // значение = 10
модуль.прибавить(5); // значение += 5
модуль.умножить(2); // значение *= 2
модуль.узнатьЗначение(); // вывести в консоль 30 (здесь надо округлить)

Также можно вызывать методы цепочкой:
модуль.установитьЗначение(10).вСтепень(2).узнатьЗначение(); // 100
 */

function modulCalc() {
  let numb = 0
  return{
    setNubmer(numberObtained = 0) {
      numb = Number.parseFloat(Number(numberObtained).toFixed(2))
      return this
    },
    getPluse(numberObtained) {
      numb += numberObtained
      return this
    },
    getMinuse(numberObtained) {
      numb -= numberObtained
      return this
    },
    getMultiply(numberObtained) {
      numb *= numberObtained
      return this
    },
    getDevide(numberObtained) {
      numb /= numberObtained
      return this
    },
    getNubmer() {
      return numb
    }
  }
}
//const calc = modulCalc()

//calc.setNubmer(10).getPluse(5).getMultiply(2).getNubmer()


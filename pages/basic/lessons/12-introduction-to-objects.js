document.title = '12-introduction-to-objects';
const user = {
  firstName: 'Denis',
  age: 30,
  isAdmin: true,
  email: 'test@test.com',
  'user-address': {
    city: 'Kharkiv'
  },
  skills: ['html', 'css', 'js']
};

let value;
let prop = 'skills';

value = user.firstName;
value = user['isAdmin'];
value = user['user-address'];
value = user['user-address'].city;
value = user['user-address']['city'];
value = user[prop][0];

user.firstName = 'Den';

value = user.firstName;

value = user.info = 'Some info';

value = user.info;

value = user['user-address'].city = 'Kiev';
value = user['user-address'].country = 'Ukraine';

// console.log(user.plan);
value = user.plan = {}; //console.log(value)
value = user.plan.basic = 'basic'; //console.log(value)
value = user.plan ; //console.log(value)
// console.log(user);
// console.log(value);

/*
///Проверка на пустоту
 */
let schedule = {};

//console.log( isEmpty(schedule) ); // true
schedule["8:30"] = "get up";
console.log(Object.keys(schedule).length)
console.log(Object.getPrototypeOf(schedule) === Object.prototype)
//console.log( isEmpty(schedule) ); // false
function isEmpty(obj){
  for (let key in obj) {
    // если тело цикла начнет выполняться - значит в объекте есть свойства
    //console.log(key)
    return false;
  }
  //console.log('obj - ', obj)
  return true;
}

/*
//Сумма свойств объекта
 */
let salaries = {
  John: 100,
  Ann: 160,
  Pete: 130
}
let sum = 0
for (let key in salaries){
  console.log(key, salaries[key])
  sum += salaries[key]
}
//console.log(sum)
/*
Умножаем все числовые свойства на 2
 */
// до вызова функции
let menu = {
  width: 200,
  height: 300,
  title: "My menu"
};
console.log(multiplyNumeric(menu))


function multiplyNumeric(obj) {
  for (let key in obj){
    if (Number(obj[key])){
       obj[key] *= 2
    }
  }
  return obj
}
//console.log(multiplyNumeric(menu))

function multiplyNumeric2(obj) {
  for (let key in obj) {
    if (typeof obj[key] == 'number') {
      obj[key] *= 2;
    }
  }
}
//console.log(multiplyNumeric2(menu))
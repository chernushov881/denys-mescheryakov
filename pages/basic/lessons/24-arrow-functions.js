document.title = '24-arrow-functions'
const plus = (x = 0, y = 0) => x + y;
const plusRes = plus(1, 2);
//console.log(plusRes)

const withoutArgs = () => console.log('Hello world');
//withoutArgs()
const singleArg = x => x * x;
//console.log(singleArg(5))
const moreActions = (a= 2 , b = 2) => {
  a *= 2;
  b *= 3;
  return a + b;
};
//console.log(moreActions())
const returnObj = (str = '') => ({
  value: str,
  length: str.length,
});
//console.log(returnObj('Hello'))
function plusFoo(x, y) {
  console.log(arguments);
  return x + y;
}

 //plusFoo(1, 2, 3, 'hello');
//console.log(plusFoo(1, 2, 3, 'hello'))

const obj = {
  firstName: 'Denis',
  age: 30,
  getFirstName() {
    console.log(this);
  },
  getAgeArrow: null,
  getAge() {
    this.getAgeArrow = () => this;
    setTimeout(() => console.log(this));
    
  },
};

// obj.getAge();
// console.log( obj.getAgeArrow())

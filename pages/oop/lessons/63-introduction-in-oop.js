document.title = '63-introduction-in-oop.js'

const sts = new String('hello world')

function Product(brand, price, discont){
    this.brand = brand
    this.price = price
    this.discont = discont
    this.getPriceWithDiscont = function(){
        return (this.price * (100 - this.discont)) / 100
    }
}

const apple = new Product('Apple', 100, 15)
const samsung = new Product('Samsung', 200, 25)

console.log(apple)
console.log(samsung)
document.title = '64-prototype-es5.js'

const sts = new String('hello world')

function Product(brand, price, discont){
    this.brand = brand
    this.price = price
    this.discont = discont
}
Product.prototype.getPriceWithDiscont = function(){
    return (this.price * (100 - this.discont)) / 100
}
Product.prototype.setPrice = function(newPrice){
    this.price = newPrice
}

const apple = new Product('Apple', 100, 15)
const samsung = new Product('Samsung', 200, 25)

apple.setPrice(300)

console.log(apple.getPriceWithDiscont)
console.log(apple)
console.log(samsung)
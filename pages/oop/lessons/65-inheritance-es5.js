document.title = '65-inheritance-es5.js'

const sts = new String('hello world')

function Product(brand, price, discont){
    this.brand = brand
    this.price = price
    this.discont = discont
}
Product.prototype.getPriceWithDiscont = function(){
    return (this.price * (100 - this.discont)) / 100
}
Product.prototype.setPrice = function(newPrice){
    this.price = newPrice
}

const apple = new Product('Apple', 100, 15)
const samsung = new Product('Samsung', 200, 25)

// Object.create
const protoForObj = {
    sayHello(){
        return 'Hello wordld'
    }
}

const obj = Object.create(protoForObj, {
    firstName: {
        value: 'Sergey'
    }
});

function User(firstName, lastName){
    this.firstName = firstName
    this.lastName = lastName
}


User.prototype.getFullName = function (){
    return `Hello ${this.firstName} ${this.lastName}`
}
const user =  new User('Serhii', 'Chernyshov')

// Customer
function Customer(firstName, lastName, nembership){
    User.apply(this, arguments)

    this.nembership = nembership
}

Customer.prototype = Object.create((User.prototype))
Customer.prototype.cunstructor = Customer

Customer.prototype.getMembersop = function(){
    return this.nembership.toUpperCase()
}

const customer = new Customer('Ivan', 'Ivanov', 'basic')

console.log(customer)
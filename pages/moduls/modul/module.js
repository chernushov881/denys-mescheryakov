let data = {
    name: 'Denis'
};

export function getData() {
    return data;
}

const symbol = Symbol();

export class User {
    constructor(firstName) {
        this[symbol] = firstName;
    }

    getFirstName() {
        return this[symbol];
    }
}
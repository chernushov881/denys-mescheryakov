document.title = '48-introduction.js'
const btn = document.querySelector('.btn-button')
const container = document.querySelector('.container-cards')
function  getPosts(cb) {
  const xhr = new XMLHttpRequest();
  xhr.open("GET", "https://jsonplaceholder.typicode.com/posts", true);
  xhr.addEventListener('load', () => {
    const response = JSON.parse(xhr.responseText)
    cb(response)
  })
  
  xhr.addEventListener('error', () => {
    console.error('error')
  })
  
  xhr.send()
}

function rerderPosts(response){
  const fragment = document.createDocumentFragment()
  response.forEach( post => {
    const card = document.createElement('div')
    card.classList.add('card')
    const body = document.createElement('div')
    body.classList.add('card-body')
    const title = document.createElement('h5')
    title.classList.add('card-title')
    title.textContent = post.text
    const article = document.createElement('div')
    article.classList.add('card-text')
    title.textContent = post.body
    body.appendChild(title)
    body.appendChild(article)
    card.appendChild(body)
    fragment.appendChild(card)
  })
  container.appendChild(fragment)
}

btn.addEventListener('click', e =>{
  getPosts( rerderPosts)
})


//div class="card" style="width: 18rem;">
//   <img class="card-img-top" src="..." alt="Card image cap">
//   <div class="card-body">
//     <h5 class="card-title">Card title</h5>
//     <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
//     <a href="#" class="btn btn-primary">Go somewhere</a>
//   </div>
// </div>
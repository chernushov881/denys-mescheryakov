document.title = 'HW Введение в DOM'
//const div = document.querySelector('div');
//const titles = document.querySelectorAll('h1');
// const h1 = document.getElementsByTagName('h1');
// console.dir(h1);
// console.log(titles);
// console.log(Array.from(titles));
// console.log(Array.prototype.slice.call(titles));
// console.log([...titles]);

/*
 Зная структуру html, с помощью изученных
 методов получить (в консоль):
 1. head
 2. body
 3. все дочерние элементы body и вывести их в
 консоль.
 4. первый div и все его дочерние узлы
 а) вывести все дочерние узлы в консоль
 б) вывести в консоль все дочерние узлы,
 кроме первого и последнего
 Для навигации по DOM использовать методы,
 которые возвращают только элементы
 */
// 1. head
//console.log(document.head)
//  2. body
//console.log(document.body)
// 3. все дочерние элементы body и вывести их в консоль.
//console.log(document.body.children)
console.log(document.body.firstElementChild);
console.log(document.body.firstElementChild.children);
// 4.б
const div = document.body.firstElementChild;
/*
 4. первый div и все его дочерние узлы
 а) вывести все дочерние узлы в консоль
 б) вывести в консоль все дочерние узлы,
 кроме первого и последнего
 Для навигации по DOM использовать методы,
 которые возвращают только элементы
 */
const childrenBody = document.body.children

if (childrenBody){
	// a
	for (let i = 0; i < childrenBody.length; i++){
		//console.log(childrenBody[i])
	}
	// b
	for (let i = 1; i < childrenBody.length - 1; i++){
			//console.log(childrenBody[i])
		}
}
// При помощи оператора ... мы получаем из коллекции массив который фильтруем сравнивая каждый элемент с первым и последним элементом в div.
const filteredArticles = [...div.children].filter(p => p !== div.firstElementChild && p !== div.lastElementChild);
console.log(filteredArticles);
/*
 1. Создать функцию, которая принимает два элемента. Функция проверяет, является ли первый элемент родителем для второго:
  isParent(parent, child);
 isParent(document.body.children[0], document.querySelector('mark'));
 // true так как первый див является родительским элементом для mark
 isParent(document.querySelector('ul'), document.querySelector('mark'));
 // false так ul НЕ является родительским элементом для mark
 Функция принимает только DOM объекты.
 */
function isParent(parent, child){
	return console.log( (child.closest(parent.tagName) == null) ? 'false' : 'true' )
}
function isParent2(parent, child){
 return console.log( parent === child.parentElement )
}

isParent2(document.body.children[0], document.querySelector('mark'))
isParent2(document.querySelector('ul'), document.querySelector('mark'));

//  2. Получить список всех ссылок, которые не находятся внутри списка ul
const linkNotList = Array.from(document.querySelectorAll('body a')).filter( link => !link.closest('ul'))
console.log(linkNotList)

//3. Найти элемент, который находится перед и после списка ul

const list = document.querySelector('ul')
const prevElementList = list.previousElementSibling
const nextElementList = list.nextElementSibling
document.title = 'event-popup'
// 1.По нажатию на кнопку "btn-msg" должен появиться алерт с тем текстом который находится в атрибуте data-text у
// кнопки.
const btn = document.querySelector('#btn-msg')
btn.addEventListener('click', function (e) {
 console.log(this.dataset.text)
 console.log(this.getAttribute('data-text'))
 console.log(e.target.getAttribute('data-text'))
 console.log(e.currentTarget.dataset.text)
 let data = this.getAttribute('data-text')
 alert(data)
})

// 2. При наведении указателя мыши на "btn-msg", кнопка становится красной; когда указатель мыши покидает кнопку, она
// становится прежнего цвета. Цвет менять можно через добавление класса.
btn.addEventListener('mouseover', function () {
 this.style.background = 'red'
})
btn.addEventListener('mouseout', function () {
 this.style.background = 'transparent'
})

// 3. При нажатии на любой узел документа показать в элементе с id=tag имя тега нажатого элемента.
const textTag = document.querySelector('#tag')
document.addEventListener('click', function (e) {
 const	enything = e.target.tagName
 textTag.textContent = enything
})
const tagTextContainer = document.getElementById('tag');
document.body.addEventListener('click', (e) => {
 tagTextContainer.textContent = `Tag: ${e.target.nodeName}`;
});
//  4. При нажатии на кнопку btn-generate добавлять в список ul элемент списка Li с текстом Item + порядковый номер Li
// по списку, т.е Item 3, Item 4 и т.д
const btn2 = document.querySelector('#btn-generate')
btn2.addEventListener('click', function (e) {
 const ul = document.querySelector('.list'),
	listNumber = ul.querySelectorAll('li').length + 1,
	fragment = document.createDocumentFragment()
 let li = document.createElement('li')
 li.textContent = `item ${listNumber + 1}`
 fragment.appendChild(li)
 ul.appendChild(fragment)
})


/*
 6. Реализовать примитивный дропдаун. Изначально все dropdown-menu скрыты через класс .d-none. При клике на dropdown-item должен отображаться блок dropdown-menu который вложен именно в тот  dropdown-item на котором произошел клик. При повторном клике на этот же dropdown-item блок dropdown-menu должен закрыться. При клике на любой другой dropdown-item уже открытый dropdown-menu должен закрываться а на тот который кликнули открываться.
 */
const menu = document.querySelector('.menu'),
 list = menu.querySelectorAll('.dropdown-menu')
menu.addEventListener('click', function(e){
 if (e.target.tagName === 'SPAN'){
  let li = e.target.closest('.dropdown-item')
  if (li.querySelector('.dropdown-menu')){
   li.querySelector('.dropdown-menu').classList.toggle('d-none')
  }
 }
})
/*
const dropdownItems = document.querySelectorAll('.dropdown-item');
// В данной переменной мы будем хранить текущее открытое меню
let currentOpenedMenu = null;

function toggleDropdownMenu(e) {
 // получаем блок меню внутри .dropdown-item
 const menu = e.currentTarget.querySelector('.dropdown-menu');
 // метод toggle возвращает булевое значение, если клас удален то вернет false а если добавлен то true. Это значение мы сохраняем в переменную
 const isAdded = menu.classList.toggle('d-none');
 // если в currentOpenedMenu уже есть блок и он не равен текущем menu то мы его скрываем
 if (currentOpenedMenu && currentOpenedMenu !== menu) {
  currentOpenedMenu.classList.add('d-none');
 }
 // Если класс d-none был удален то значет меню видимо и мы его сохраняем в переменную currentOpenedMenu
 if (!isAdded) {
  currentOpenedMenu = menu;
 }
}

dropdownItems.forEach(d => d.addEventListener('click', toggleDropdownMenu));
 */
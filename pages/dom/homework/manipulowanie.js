document.title = 'HW Манипуляция DOM элементами'
// 1. Найти параграф и получить его текстовое содержимое (только текст!)
const p = document.querySelector('p')
console.log(p.textContent)


//  2. Создать функцию, которая принимает в качестве аргумента узел DOM и возвращает информацию (в виде объекта) о типе
// узла, об имени узла и о количестве дочерних узлов (если детей нет - 0).
function infoDom(elem) {
 if (!(node instanceof Element)) return;
 
 return {
	type: elem.nodeType,
	name: elem.nodeName,
	child: elem.hasChildNodes() ? elem.childNodes : 0
 }
}

//console.log(infoDom(document.querySelector('p')))

/*
 3. Получить массив, который состоит из текстового содержимого ссылок внутри списка: getTextFromUl(ul) ---> ["Link1", "Link2", "Link3"]
 */
function getTextFromUl(elem) {
 if (!(elem instanceof HTMLElement)) return;
 const list = elem.querySelectorAll('li')
 return [...list].map(e => e.textContent)
}

const ul = document.querySelector('ul')
getTextFromUl(ul)

/*
 4. В параграфе заменить все дочерние текстовые узлы на “-text-” (вложенные теги должны остаться). Конечный результат:
 -text-<a href="#">reprehendunt</a>-text-<mark>nemore</mark>-text- */
const p2 = document.querySelector('p a')
p2.insertAdjacentText('beforebegin', '-text-')
p2.insertAdjacentElement

/*
* Атрибуты элементов.
 */
//1. Найти в коде список ul и добавить класс “list”
const ul3 = document.querySelector('ul')
ul3.classList.add('list')

// 2. Найти в коде ссылку, находящуюся после списка ul, и добавить id=link
const link = document.querySelector('ul ~ a')
link.id = 'link'
/*
 const list = document.querySelector('ul');
 // Будущая ссылка
 let link;
 // Следующий элемент после списка, стартовая точка.
 let nextElement = list.nextElementSibling;
 // Проверяем пока не найдена ссылка или больше не осталось следующих элементов мы выполняем данный цикл.
 while(!link || !nextElement) {
 // Если больше нет следующего элемента мы останавливаем цикл
 if (!nextElement) break;
 // Если у следующего элемента тег ссылка то мы записываем ее в переменную
 if (nextElement.tagName === 'A') {
 link = nextElement;
 }
 // записываем следующий элемент
 nextElement = nextElement.nextElementSibling;
 
 */
// 3. На li через один (начиная с самого первого) установить класс “item”

function addListItem() {
 const [...li] = ul.querySelectorAll('li')
 return li.map((e, i) => {
	i % 2 ? e.classList.add('item') : e
 })
}

addListItem()

/*
 4. На все ссылки в примере установить класс “custom-link”
 Код для задач брать со слайда 4.
 */
const [...listAll] = document.querySelectorAll('a')

listAll.forEach(e => e.classList.add('custom-link'))
/*
* Создание и добавление узлов и элементов.
*/
/*
 1. Не используя innerHTML, добавить в список несколько li с классом ‘new-item’ и текстом ‘item’ + номер li:
 <ul>
 <li><a href="#">Link1</a></li>
 ...
 <li class=”new-item”>item 5</li>
 <li class=”new-item”>item 6</li>
 </ul>
 Вручную номер li не ставить оно должно подставляться в зависимости от кол-ва лишек в списке.
*/
function addList(number) {
 const ul = document.querySelector('ul'),
	liLength = ul.querySelectorAll('li').length + 1,
	fragment = document.createDocumentFragment()
 for (let i = 0; i < number; i++) {
	let li = document.createElement('li')
	li.textContent = `item ${liLength + i}`
	li.classList.add('new-item')
	fragment.appendChild(li)
 }
 ul.appendChild(fragment)
}
addList(2)

//  2. В каждую ссылку, которая находятся внутри списка ul  добавить по тегу strong (в каждую ссылку один - strong).
function addTagStorng(){
 const [...links] = document.querySelectorAll('ul a');
 links.forEach(link => link.insertAdjacentHTML('beforeend', '<strong>Strong</strong>'));
}
addTagStorng()

// 3. В начало документа (в начало body) добавить картинку img с атрибутами src и alt (текст придумайте сами). В src добавьте реальный url к картинке. Для создания элемента используйте метод createElement.
const img = document.createElement('img')
img.src = '/img/is-img.png'
img.title = 'JavaScript, w skrócie JS – skryptowy oraz wieloparadygmatowy język programowania'
img.setAttribute('src', 'https://via.placeholder.com/150/92c952');
img.setAttribute('alt', 'some image');
//document.body.appendChild(img)
document.body.insertAdjacentElement('afterbegin', img)

// 4. Найти на странице элемент mark, добавить в конец содержимого текст “green” и на элемент установить класс green
const mark = document.querySelector('mark')
mark.classList.add('green')
mark.insertAdjacentText('beforeend',' green')
mark.insertAdjacentElement

// 5. Отсортировать li внутри списка в обратном порядке (по тексту внутри)

//[...list].map( e => {

function reverseList(){
 const list = document.querySelector('ul');
 // Сортируем массив элементов по текстовому содержимому
 const listItems = [...list.children].sort((prev, next) => {
	return prev.textContent > next.textContent ? -1 : 1;
 });
 // Очищаем текущее содержимое списка
 list.innerHTML = '';
 // Генерируем новое содержимое из отсортированного массива элементов
 listItems.forEach((item) => list.appendChild(item));
}
reverseList()


//return ul.insertAdjacentElement
/**/
/*console.log(list[0])
 let newList = [...list].map(e => {
 console.log(e)
 ul.insertAdjacentElement += ('afterbegin', e)
 })
 console.log(newList)*/

// function getTextFromUl(elem) {
//  const list = elem.querySelectorAll('li')
//  return [...list].map(e => e.textContent)
// }
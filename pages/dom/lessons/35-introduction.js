document.title = 'L Введение в DOM'

const div = document.querySelector('div');
const titles = document.querySelectorAll('h1');
// const h1 = document.getElementsByTagName('h1');
// console.dir(h1);
// console.log(titles);
// console.log(Array.from(titles));
// console.log(Array.prototype.slice.call(titles));
// console.log([...titles]);

var divs = document.getElementsByTagName('div');
for (let i = 0; i < divs.length; i++) {
	if (divs[i].matches('.box')) {
		console.log(divs[i]);
	}
} // выведет в консоль все дивы с классом box


console.log(document.querySelector('.mark').closest('div'))
// найдёт первый элемент с классом mark и получит ближайший обрамляющий тег div

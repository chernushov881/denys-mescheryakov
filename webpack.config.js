let path = require('path');

let conf = {
    //entry: './pages/number-guessing-game.js',
    //entry: './pages/basic/homeWork/number.js',
    //entry: './pages/basic/homeWork/string.js',
    //entry: './pages/basic/lessons/14-introduction-to-objects.js',
    //entry: './pages/basic/homeWork/if-statement.js',
    //entry: './pages/basic/homeWork/cyrcle.js',
    //entry: './pages/basic/homeWork/switch-case.js',
    //entry: './pages/basic/homeWork/objects.js',
    //entry: './pages/basic/lessons/20-cyrcle.js',
    //entry: './pages/basic/homeWork/function.js',
    //entry: './pages/basic/homeWork/top-order-function.js',
    //entry: './pages/basic/lessons/23-this.js',
    //entry: './pages/basic/homeWork/this.js',
    //entry: './pages/basic/homeWork/apprrow-functions.js',
    //entry: './pages/basic/lessons/25-sortingArrayMethods.js',
    //entry: './pages/basic/homeWork/sortingArrayMethods.js',
    //entry: './pages/basic/lessons/26-callback.js',
    //entry: './pages/basic/homeWork/callback.js',
    //entry: './pages/basic/lessons/27-methodthObjects.js',
    //entry: './pages/basic/lessons/28-destructuring.js',
    //entry: './pages/basic/lessons/29-object-descriptor.js',
    //entry: './pages/basic/lessons/30-wrapper-objects.js',
    //entry: './pages/dom/lessons/35-introduction.js',
    //entry: './pages/dom/homeWork/introduction.js',
    //entry: './pages/dom/lessons/36-attributes-elements.js',
    //entry: './pages/dom/lessons/37-manipulowanie.js',
    //entry: './pages/dom/homeWork/manipulowanie.js',
    //entry: './pages/dom/lessons/38-event.js',
    //entry: './pages/dom/lessons/39-event-popup.js',
    //entry: './pages/dom/lessons/39-event-example.js',
    //entry: './pages/dom/homeWork/event-popup.js',
    //entry: './pages/dom/lessons/40-todo-list.js',
    //entry: './pages/dom/homeWork/todo.js',
    //entry: './pages/dom/lessons/43-todolist-chenge-themes.js',
    //entry: './pages/dom/lessons/44-localStorage.js',
    //entry: './pages/asynchrony/lessons/48-introduction.js',
    //entry: './pages/asynchrony/homeWork/introduction.js',
    //entry: './pages/asynchrony/lessons/50-post-requests.js',
    //entry: './pages/asynchrony/homeWork/post-requests.js',
    //entry: './pages/asynchrony/lessons/52-cors.js',
    //entry: './pages/asynchrony/lessons/53-error-processing.js',
    //entry: './pages/asynchrony/lessons/55-news-app.js',
    //entry: './pages/oop/lessons/63-introduction-in-oop.js',
    //entry: './pages/oop/lessons/64-prototype-es5.js',
    //entry: './pages/oop/lessons/65-inheritance-es5.js',
    //entry: './pages/oop/lessons/66-class.js',
   // entry: './pages/moduls/lessons/69-import-export.js',
    //entry: './pages/moduls/lessons/71-incapsulation.js',
    entry: './pages/moduls/lessons/72-incapsulation.js',
    output: {
        path: path.resolve(__dirname, './js'),
        filename: 'main.js',
        publicPath: 'js/'
    },
    devServer: {
        overlay: true
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                // exclude: '/node_modules/'
            }
        ]
    }
};

module.exports = (env, options) => {
    conf.devtool = options.mode === "production" ?
        false :
        "cheap-module-eval-source-map";

    return conf;
};